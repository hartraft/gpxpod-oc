OC.L10N.register(
    "gpxpod",
    {
    "GpxPod" : "GpxPod",
    "There was an error during \"gpxelevations\" execution on the server" : "Došlo k chybě při spuštění \"gpxelevations\" na serveru",
    "GpxPod public access" : "Veřejný přístup GpxPod",
    "Public file access" : "Přístup k veřejným souborům",
    "Public folder access" : "Přístup k veřejné složce",
    "Directory {p} has been added" : "Adresář {p} byl přidán",
    "Failed to add directory" : "Selhalo vytvoření adresáře",
    "View in GpxPod" : "Zobrazit v GpxPod",
    "Elevation" : "Nadmořská výška",
    "Speed" : "Rychlost",
    "Pace" : "Tempo",
    "Close elevation chart" : "Zavřít graf převýšení",
    "File" : "Soubor",
    "download" : "stáhnout",
    "This public link will work only if '{title}' or one of its parent folder is shared in 'files' app by public link without password" : "Tento veřejný odkaz bude fungovat pouze v případě, že '{title}' nebo je jedna z nadřazených složek sdílena v aplikaci 'soubory' podle veřejného odkazu bez hesla",
    "Draw track" : "Nakreslit trasu",
    "metadata link" : "odkaz na metadata",
    "tracks/routes name list" : "seznam názvů stop/tras",
    "Distance" : "Vzdálenost",
    "Duration" : "Doba trvání",
    "Moving time" : "Čas pohybu",
    "Pause time" : "Čas pozastavení",
    "Begin" : "Počátek",
    "End" : "Konec",
    "Cumulative elevation gain" : "Kumulativní nárůst převýšení",
    "Cumulative elevation loss" : "Kumulativní snížení nadmořské výšky",
    "Minimum elevation" : "Minimální nadmořská výška",
    "Maximum elevation" : "Maximální nadmořská výška",
    "Maximum speed" : "Maximální rychlost",
    "Average speed" : "Průměrná rychlost",
    "Moving average speed" : "Průměrná rychlost pohybu",
    "Moving average pace" : "Průměrné tempo",
    "Track \"{tn}\" is loading" : "Stopa \"{tn}\" se načítá",
    "Failed to delete track" : "Nepodařilo se odstranit stopu",
    "Reload this page" : "Načíst znovu tuto stránku",
    "Error" : "Chyba",
    "Successfully deleted" : "Úspěšně smazáno",
    "You can restore deleted files in \"Files\" app" : "Smazané soubory můžete obnovit v aplikaci \"Soubory\"",
    "Impossible to delete" : "Není možné odstranit",
    "Failed to delete selected tracks" : "Nepodařilo se odstranit vybrané stopy",
    "Click the color to change it" : "Klepněte na barvu pro změnu",
    "Deselect to hide track drawing" : "Zrušit výběr pro skrytí kresby trasy",
    "Select to draw the track" : "Vyberte pro nakreslení trasy",
    "More" : "Více",
    "Center map on this track" : "Vycentrovat mapu na tuto trasu",
    "Delete this track file" : "Odstranit tento soubor trasy",
    "Correct elevations with smoothing for this track" : "Oprava nadmořské výšky vyhlazením pro tuto stopu",
    "Correct elevations for this track" : "Oprava nadmořské výšky pro tuto stopu",
    "View this file in GpxMotion" : "Zobrazit tento soubor v GpxMotion",
    "Edit this file in GpxEdit" : "Upravit tento soubor v GpxEdit",
    "no date" : "žádné datum",
    "No track visible" : "Žádná stopa není viditelná",
    "Tracks from current view" : "Stopy z aktuálního zobrazení",
    "All tracks" : "Všechny stopy",
    "Draw" : "Kreslit",
    "Track" : "Stopa",
    "Date" : "Datum",
    "Dist<br/>ance<br/>" : "<br/>Vzdálenost<br/>",
    "Cumulative<br/>elevation<br/>gain" : "Kumulativní přírůstek<br/>převýšení<br/>",
    "Total" : "Celkem",
    "distance" : "vzdálenost",
    "altitude/distance" : "nadmořská výška/vzdálenost",
    "speed/distance" : "rychlost/vzdálenost",
    "pace(time for last km or mi)/distance" : "tempo (čas za poslední km nebo mi)/vzdálenost",
    "Link" : "Odkaz",
    "Latitude" : "Zeměpisná šířka",
    "Longitude" : "Zeměpisná délka",
    "Comment" : "Komentář",
    "Description" : "Popis",
    "Symbol name" : "Název symbolu",
    "Database has been cleaned" : "Databáze byla vyčištěna",
    "Impossible to clean database" : "Není možné vyčistit databázi",
    "Folder" : "Složka",
    "Public link to '{folder}' which will work only if this folder is shared in 'files' app by public link without password" : "Veřejný odkaz na '{folder}', který bude fungovat pouze v případě, že je tato složka sdílena v aplikaci 'soubory' veřejným odkazem bez hesla",
    "Public folder share" : "Sdílení veřejné složky",
    "Public file share" : "Sdílení veřejného souboru",
    "Server name or server url should not be empty" : "Název serveru nebo adresa serveru by neměly být prázdné",
    "Impossible to add tile server" : "Nelze přidat mapový server",
    "A server with this name already exists" : "Server s tímto názvem již existuje",
    "Delete" : "Smazat",
    "Tile server \"{ts}\" has been added" : "Byl přidán mapový server \"{ts}\"",
    "Failed to add tile server \"{ts}\"" : "Nepodařilo se přidat mapový server \"{ts}\"",
    "Tile server \"{ts}\" has been deleted" : "Mapový server \"{ts}\" byl smazán",
    "Failed to delete tile server \"{ts}\"" : "Nepodařilo se odebrat mapový server \"{ts}\"",
    "Failed to restore options values" : "Nepodařilo se obnovit hodnoty možností",
    "Failed to save options values" : "Nepodařilo se uložit hodnoty možností",
    "Destination directory is not writeable" : "Cílový adresář není zapisovatelný",
    "Destination directory does not exist" : "Cílový Adresář neexistuje",
    "Failed to move selected tracks" : "Nepodařilo se přesunout vybrané stopy",
    "Following files were moved successfully" : "Následující soubory byly úspěšně přesunuty",
    "Following files were NOT moved" : "Následující soubory NEJSOU přesunuty",
    "There is no compatible file in {p} or any of its sub directories" : "Neexistuje žádný kompatibilní soubor v {p} ani v žádném z jeho podadresářů",
    "Failed to recursively add directory" : "Nepodařilo se rekurzivně přidat adresář",
    "Directory {p} has been removed" : "Adresář {p} byl odstraněn",
    "Failed to remove directory" : "Nepodařilo se odstranit adresář",
    "Public link to the track" : "Veřejný odkaz na trasu",
    "Public link to the folder" : "Veřejný odkaz na složku",
    "Select at least one track" : "Vyberte alespoň jednu stopu",
    "Destination folder" : "Cílová složka",
    "Origin and destination directories must be different" : "Počáteční a cílové adresáře se musí lišit",
    "Are you sure you want to delete the track {name} ?" : "Jste si jisti, že chcete odstranit stopu {name}?",
    "Confirm track deletion" : "Potvrdit odstranění trasy",
    "Add directory" : "Přidat adresář",
    "Add directory recursively" : "Přidat adresář rekurzivně",
    "better in" : "lepší v",
    "worse in" : "horší v",
    "Divergence details" : "Podrobnosti o rozdílech",
    "Divergence distance" : "Rozdílová vzdálenost",
    "is shorter than" : "je kratší než",
    "is longer than" : "je delší než",
    "is quicker than" : "je rychlejší než",
    "is slower than" : "je pomalejší než",
    "is less than" : "je menší než",
    "is more than" : "je větší než",
    "There is no divergence here" : "Neexistuje zde žádný rozdíl",
    "Segment details" : "Podrobnosti segmentu",
    "Segment id" : "Id segmentu",
    "From" : "Od",
    "To" : "Do",
    "Time" : "Čas",
    "Click on a track line to get details on the section" : "Kliknutím na trasu zobrazíte podrobnosti v sekci",
    "Folder and tracks selection" : "Výběr složky a stop",
    "Settings and extra actions" : "Nastavení a další akce",
    "About GpxPod" : "O GpxPod",
    "Choose a folder" : "Zvolte složku",
    "Reload and analyze all files in current folder" : "Znovu načíst a analyzovat všechny soubory v aktuální složce",
    "Reload current folder" : "Znovu načíst aktuální složku",
    "Add directories recursively" : "Přidat rekurzivně adresáře",
    "Delete current directory" : "Odstranit aktuální adresář",
    "There is no directory in your list" : "Ve vašem seznamu není žádný adresář",
    "Options" : "Možnosti",
    "Map options" : "Možnosti mapy",
    "Display markers" : "Zobrazit značky",
    "Show pictures markers" : "Zobrazit obrázky značek",
    "Only pictures with EXIF geolocation data are displayed" : "Zobrazeny jsou pouze obrázky s EXIF geolokačními daty",
    "Show pictures" : "Zobrazit obrázky",
    "With this disabled, public page link will include option to hide sidebar" : "S tímto vypnutím bude veřejný odkaz na stránku obsahovat možnost skrýt postranní panel",
    "Enable sidebar in public pages" : "Povolit postranní panel na veřejných stránkách",
    "Open info popup when a track is drawn" : "Otevřít informační vyskakovací okno při vykreslování stopy",
    "Auto-popup" : "Auto-popup",
    "If enabled :" : "Pokud je povoleno:",
    "Zoom on track when it is drawn" : "Přiblížit na stopu při kreslení",
    "Zoom to show all track markers when selecting a folder" : "Přiblížit pro zobrazení všech značek stopy při výběru složky",
    "If disabled :" : "Pokud je zakázáno:",
    "Do nothing when a track is drawn" : "Nedělat nic při kreslení stopy",
    "Reset zoom to world view when selecting a folder" : "Obnovit přiblížení do zobrazení celé mapy při výběru složky",
    "Auto-zoom" : "Automatické přiblížení",
    "Display elevation or speed chart when a track is drawn" : "Zobrazit graf převýšení nebo rychlosti při kreslení stopy",
    "Display chart" : "Zobrazit graf",
    "Send HTTP referrer (need reload)" : "Odeslat HTTP referrer (je třeba znovu načíst)",
    "Timezone" : "Časové pásmo",
    "Measuring units" : "Jednotky měření",
    "Metric" : "Metrické",
    "English" : "anglické",
    "Nautical" : "Námořní",
    "Gpx symbols" : "Gpx symboly",
    "Line width" : "Tloušťka čáry",
    "Display routes points" : "Zobrazit body trasy",
    "Direction arrows" : "Směrové šipky",
    "tracks" : "stopy",
    "routes" : "trasy",
    "waypoints" : "body trasy",
    "Waypoint style" : "Styl trasového bodu",
    "Tooltip" : "Popisek nástroje",
    "on hover" : "při najetí kurzoru",
    "permanent" : "trvalé",
    "speed" : "rychlost",
    "elevation" : "nadmořská výška",
    "pace" : "tempo",
    "Pressure" : "Tlak",
    "GNSS" : "GNSS",
    "Dynamic table" : "Dynamická tabulka",
    "Simplified previews" : "Zjednodušené náhledy",
    "Transparency" : "Průhlednost",
    "Display shared folders/files" : "Zobrazit sdílené složky/soubory",
    "Explore external storages" : "Prozkoumat externí úložiště",
    "Deselect all" : "Odznačit vše",
    "Compare selected tracks" : "Porovnat vybrané stopy",
    "Filters" : "Filtry",
    "Apply" : "Použít",
    "Style.json url" : "url adresa style.json",
    "Transparent" : "Průhledné",
    "Opacity (0.0-1.0)" : "Průhlednost (0.0-1.0)",
    "Layers to display" : "Vrstvy k zobrazení",
    "deleting" : "mazání",
    "Shortcuts" : "Klávesové zkratky",
    "toggle sidebar" : "vyp/zap. postranní panel",
    "toggle minimap" : "vyp/zap. minimapu",
    "Features" : "Funkce",
    "Documentation" : "Dokumentace",
    "Source management" : "Správa zdrojů",
    "Authors" : "Autoři",
    "Upload gpx files to compare" : "Nahrát soubory gpx pro porovnání",
    "Compare" : "Porovnat",
    "File pair to compare" : "Dvojice souborů pro porovnání",
    "and" : "a"
},
"nplurals=4; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 3;");
